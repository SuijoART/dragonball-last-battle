## Informations

| Captures d'écran / Screenshots | |
| :----: | :----: |
| [<img src="./screenshots/img00.png" width="320" height=240/>](./screenshots/img00.png) | [<img src="./screenshots/img01.png" width="320" height=240/>](./screenshots/img01.png) |

| 📝 Disponibilité  / Avaibility | 💻 Système d'exploitation / Operating System |
| :-----: | :-----: |
| ❌ | *MacOS* |
| ✅​ | *GNU/Linux* |
| ✅​ | *Microsoft Windows* |

| 🔢 Version | 📅 Date de sortie / Release date | 🔗 Lien de téléchargement / Download link
| :-----: | :-----: | :-----: |
| *v1.0* | *Dec/2020* | https://gitlab.com/SuijoART/dragonball-last-battle/-/releases/v1.0 |
| *v2.0*​ | *Dec/2023* | https://gitlab.com/SuijoART/dragonball-last-battle/-/releases/v2.0 |
| *v3.0*​ | *...* | *...* |

## FR :flag_fr:

Ce projet personnel est un jeu de versus fighting basé sur l'univers du célèbre manga Dragon Ball d'Akira Toriyama.

Il s'intitule **DRAGON BALL - LAST BATTLE** et a été réalisé avec le moteur de jeu suivant : **M.U.G.E.N** *(Elecbyte)*

#### Installation Windows

Pour pouvoir y jouer, merci de choisir et  télécharger l'une des versions disponibles puis de l'extraire. 

Avant de lancer le jeu, je vous recommande de lire le fichier **documentation.pdf**.

#### Installation GNU/Linux

Suivez les instructions d'installation Windows.
Après cela, télécharger le logiciel [Wine](https://wiki.winehq.org/Download) pour pouvoir exécuter des fichiers avec une extension **EXE**. 

Enfin, dans le terminal à la racine du projet, lancer le jeu en exécutant la commande ci-dessous :

```console
wine dragonball-last-battle.exe
```

## EN :flag_gb:

This personal project is a versus fighting game based on the universe of the famous manga Dragon Ball by Akira Toriyama.

It's titled as **DRAGON BALL - LAST BATTLE** and was realized with the following game engine : **M.U.G.E.N** *(Elecbyte)*

#### Windows installation

To play it, please choose and download one of the available versions, then extract it.

Before launching the game, I recommand you to read the **documentation.pdf** file.

#### GNU/Linux installation

Follow the Windows installation instructions.
After that, download the [Wine](https://wiki.winehq.org/Download) software to be able to run files with an **EXE** extension.

Finally, in the terminal at the root of the project, launch the game by executing the following command :

```console
wine dragonball-last-battle.exe
```

