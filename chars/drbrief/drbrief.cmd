;-| Remap |------------------------------------------------------

[Remap]
a = a
b = b
x = x
y = y

;-| Defaults |------------------------------------------------------

[Defaults]
command.time = 15
command.buffer.time = 1

;===========================================================================

;-| Basic Moves |------------------------------------------------------

[Command]
name = "Walking Forwards";Required (do not remove)
command = /$F
time = 1

[Command]
name = "Walking Backwards";Required (do not remove)
command = /$B
time = 1

[Command]
name = "Running Forward"     ;Required (do not remove)
command = F, F
time = 10

[Command]
name = "Running Backward"     ;Required (do not remove)
command = B, B
time = 10

[Command]
name = "Jumping" ;Required (do not remove)
command = /$U
time = 1

[Command]
name = "Crouching";Required (do not remove)
command = /$D
time = 1

[Command]
name = "Recovery";Required (do not remove)
command = x+y
time = 1

;-| Basic Attacks |------------------------------------------------------

[Command]
name = "Ki Blast"
command = a
time = 1

[Command]
name = "Teleportation"
command = b
time = 1

;-| Special Attacks |------------------------------------------------------

[Command]
name = "Special Attack"
command = /$D, a

;-| Super Attack |------------------------------------------------------

[Command]
name = "Super Attack"
command = /$D, b

;===========================================================================

[Statedef -1]

;---------------------------------------------------------------------------
;Running Forward

[State -1, Running Forward]
type = ChangeState
value = 100
trigger1 = command = "Running Forward"
trigger1 = statetype = S
trigger1 = ctrl

;---------------------------------------------------------------------------
;Running Backward

[State -1, Running Backward]
type = ChangeState
value = 105
trigger1 = command = "Running Backward"
trigger1 = statetype = S
trigger1 = ctrl

;---------------------------------------------------------------------------
;Standing Combo

[State -1, Ki Blast]
type = ChangeState
value = 200
triggerall = command = "Ki Blast"
trigger1 = statetype = S
trigger1 = ctrl

;---------------------------------------------------------------------------
;Standing Combo

[State -1, Teleportation]
type = ChangeState
value = 300
triggerall = command = "Teleportation"
trigger1 = statetype = S
trigger1 = ctrl

;---------------------------------------------------------------------------
;Air Combo

[State -1, Air Combo]
type = ChangeState
value = 400
triggerall = command = "Ki Blast" || command = "Teleportation"
trigger1 = statetype = A
trigger1 = ctrl

;---------------------------------------------------------------------------
;Special Attack 1

[State -1, Special Attack]
type = ChangeState
value = 1000
triggerall = command = "Special Attack"
triggerall = power >= 1000
trigger1 = ctrl
trigger1 = statetype != A

;---------------------------------------------------------------------------
;Super Attack 

[State -1, Super Attack]
type = ChangeState
value = 1500
triggerall = command = "Super Attack"
triggerall = power = 3000
trigger1 = ctrl
trigger1 = statetype != A



